![JavaStore logo](https://bitbucket.org/maxtag/javastore/raw/ece146da1b331954beb6cc51c441279cd1943486/logo.PNG "JavaStore logo")

# JavaStore
Projet de programmation ayant pour thème la modélisation d'une application Java permettant la gestion d'un magasin de vente d'articles informatiques.
[Java] [AWT] [Swing] [Interface]

# Requis
- L'ensemble des fichiers .jar de la bibliothèque [JFreeChart] doit être référencé et se trouve sous le répértoire [lib/] et alternativement à l'adresse https://sourceforge.net/projects/jfreechart/.
- Le dossier [save/] et son contenu doit impérativement se situer dans le même répertoire que le [.jar] exécutable, faute de quoi le programme ne marchera pas.

# Utilisation
Vous trouverez des instructions détaillées pour vous connecter aux différentes interfaces dans le fichier [Rapport.pdf].
Pour plus de clarté, le diagramme UML du projet a été joint ci dessus.

# Auteur
Ce projet et l'integralité de ses classes et de ses interfaces graphiques sont la création de [Massil Taguemout](mailto:maxtag@vivaldi.net).  
Université d'Evry-Val-d'Essonne  
2019
